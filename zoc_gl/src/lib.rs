// See LICENSE file for copyright and license details.

include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));

// vim: set tabstop=4 shiftwidth=4 softtabstop=4 expandtab:
