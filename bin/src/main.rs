
extern crate zoc;

fn main() {
    zoc::main();
}

// vim: set tabstop=4 shiftwidth=4 softtabstop=4 expandtab:
